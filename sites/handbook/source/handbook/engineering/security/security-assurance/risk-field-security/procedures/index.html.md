---
layout: handbook-page-toc
title: "GitLab SOC2 Response Process"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}
[Security Team](/handbook/engineering/security/) > [Field Security Team Page](/handbook/engineering/security/security-assurance/risk-field-security/index.html)

## Customer Security Assessments
- [Processing SOC2 Requests](/handbook/engineering/security/security-assurance/risk-field-security/procedures/SOC2.html)
- [Using GitLab AnswerBase](/handbook/engineering/security/security-assurance/risk-field-security/procedures/gitlab-answerbase.html)
