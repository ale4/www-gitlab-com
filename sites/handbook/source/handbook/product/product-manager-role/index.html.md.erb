---
layout: handbook-page-toc
title: The Product Manager Role at GitLab
---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

On this page, you'll find an overview as well as links to helpful resources for working as a product manager at GitLab. To better understand how we evaluate a product manager's work at GitLab, please visit [Product Management CDF and Competencies](/handbook/product/product-manager-role/product-CDF-competencies/)

[**Principles**](/handbook/product/product-principles/) - [**Processes**](/handbook/product/product-processes/) - [**Categorization**](/handbook/product/categories/) - [**GitLab the Product**](/handbook/product/gitlab-the-product) - [**Being a PM**](/handbook/product/product-manager-role) - [**Performance Indicators**](/handbook/product/performance-indicators/) - [**Leadership**](/handbook/product/product-leadership/)

## Product Organizational Structure

The GitLab Product team includes team members at various levels of [Product Management job titles](/job-families/product/product-manager/) and [Product Management - Leadership job titles](job-families/product/product-management-leadership/). They map across our [organizational levels](/company/team/structure/#levels) with scope at various points in our [product hierarchy](/handbook/product/categories/#hierarchy) outlined in the table below.

* The product org abides by [GitLab's layer structure](/company/team/structure/#layers). Sometimes, there can be instances where peers across layers don't have the same title.

| Level | Job Titles| Hierarchy Scopes |
| ----- | ------------ | ---------------- |
| IC | [Product Manager](/job-families/product/product-manager/#intermediate-product-manager), [Senior PM](/job-families/product/product-manager/#senior-product-manager), [Principal PM](/job-families/product/product-manager/#principal-product-manager) | Group, Stage |
| Manager | [Group Manager Product](/job-families/product/product-management-leadership/#group-manager-product-gmp), [Director of Product](/job-families/product/product-management-leadership/#director-of-product-management) | Collection of Groups, Stage, Section |
| Director | [Director of Product](/job-families/product/product-management-leadership/#director-of-product-management), [Senior Director of Product](/job-families/product/product-management-leadership/#senior-director-of-product-management) | Section, Collection of Sections |
| Senior Leader | [VP](/job-families/product/product-management-leadership/#vp-of-product-management) | All Sections |
| Executive | [Chief Product Officer]() | Entire Function |

***

## Working as a PM at GitLab

### Responsibilities

Your job as a PM is outlined in [Product Manager Reponsibilities](https://about.gitlab.com/handbook/product/product-manager-responsibilities/)

### Getting started as a PM at GitLab

The first thing to do is to familiarize yourself with the following handbook pages

* [Product Principles](/handbook/product/product-principles)
* [Product Processes](/handbook/product/product-processes)
* [Product Manager Reponsibilities](/handbook/product/product-manager-responsibilities/)
* [Product Manager Career Development Framework](/handbook/product/product-manager-role/product-CDF-competencies/)
* [Product Development Flow](/handbook/product-development-flow/)
* [Product Development Timeline](/handbook/engineering/workflow/#product-development-timeline)
* [Product Management Learning & Development](/handbook/product/product-manager-role/learning-and-development/)
* [GitLab Values](/handbook/values/)

As a GitLab Product Manager, your product scope is huge! It may seem daunting at first, but GitLab is constantly iterating on our processes and organization so that you can be successful. Since everything is in draft, please [make a proposal](/handbook/values/#make-a-proposal) to improve things.

As a PM you may be extremely busy if you try to do everything that people ask you to do. This is not our expectation for the role. 
We do expect you to prioritize expertly and be a [manager of one](/handbook/values/#managers-of-one). And remember what your [core responsibilities](/handbook/product/product-manager-responsibilities/#core-pm-responsibilities) are! If you find yourself drawning in To-Dos, take a step back, prioritize, push-back, and focus your energy on the most important things.

Some helpful reminders:

* As a PM, you're the person that has to kick-off new initiatives. You're not
responsible for shipping something on time, but you _are_ responsible for taking
action and setting the direction. Be active everywhere, over-communicate, and
sell the things you think are important to the rest of the team and community.
* As a PM, you need to set the bar for engineering. That is, to push engineering and
the rest of the company. You almost want engineering to complain about the pace
that product is setting. Our default instinct will be to slow down, but we can't
give in to that.
* As a PM you don't own the product; ask other people for feedback and give
team members and the community the space to suggest and create things without your
direct intervention. It's your job to make sure things are decided and
planned, not come up with every idea or change.

### Where should you look when you need help?

- The first thing you should do is read this page carefully and the pages linked from this page. 
- Most answers can also be found in the [handbook](/handbook/). If you can't find the answer, please add a proposal!
- You can ask questions related to Product in the `#product` Slack channel.
- General questions should be asked in `#questions`.
- Specific Git related questions should be asked in `#git-help`.
- If you have problems with a MR, ask in `#mr-buddies`.
- HR questions should be asked in `#peopleops`.
- Anything Release Post related can be found in the [Release Post handbook](/handbook/marketing/blog/release-posts/#pm-contributors) and `#release-post`

### Job Requirements

The job requirements and expectations for [Product Managers, Sr. Product Managers, and Principal Product Managers](/job-families/product/product-manager/), [Group Manager, Product Management](/job-families/product/product-management-leadership/#group-manager-product-gmp)
[Director of Product](/job-families/product/product-management-leadership/#director-of-product-management) and
[VP of Product](/job-families/product/product-management-leadership/#vp-of-product-management) are outlined in our job families pages.

As product managers progress in their product career, we encourage our product managers to take on new challenges by moving to new product areas. Alternatively, IC PMs are encouraged to apply for roles in the management track and vice versa.

The progression of responsibilities allocation between tactical, operational and strategic in product roles
is well illustrated by this chart.

![GitLab PM Responsibility Allocation Chart](/handbook/product/pm-allocation.png)

_[Source File](https://docs.google.com/spreadsheets/d/19gAgPJVdXfBpXiFOlT1WqZLJB_eFU7W7slmAzodEuDM/edit#gid=1012729771). Note - Thanks to [Melissa Perri](https://twitter.com/lissijean/) for the inspiration_

### Important dates PMs should keep in mind

**4th of the Month:**

Draft of the issues that will be included in the next released (released 22nd of next month).
Start capacity and technical discussions with engineering/UX.

**12th of the Month:**

Release scope is finalized. In-scope issues marked with milestone
Kickoff document is updated with relevant items to be included.

**15th of the Month:**

Group Kickoffs calls recorded and uploaded by the end of the day.

Also see [Product Development Timeline](/handbook/engineering/workflow/#product-development-timeline).

### Scope of responsibilities

The product team is responsible for iteration on most of GitLab's products and
projects:

- GitLab CE and EE
- GitLab.com
- about.gitlab.com
- customers.gitlab.com
- version.gitlab.com
- license.gitlab.com

This includes the entire stack and all its facets. The product team needs to
weigh and prioritize not only bugs, features, regressions, performance, but also
architectural changes and other aspects required for ensuring GitLab's excellence

## Learning and Development for Product Management

We have a library dedicated to collecting and highlighting the best resources to support the growth and success of product managers in doing their job at GitLab as well as beyond, to achieve their long term career goals. We encourage product managers to work with their managers to identify areas for improvement, and leverage the [learning and development for product management](./learning-and-development/) resources accordingly.

## Evaluating External Roles
As members of a prolific, product-focused company, GitLab Product Managers are frequently approached with job offers at other companies. Below is a list of criteria to consider when evaluating those roles:
- **Culture:** Consider some of the tangible benefits of culture such as productivity, autonomy, transparency, and remote flexibility.
- **Product System:** Consider how well the company ships today, what the PM process is and how much autonomy Product Managers are given to do their primary role.
- **Role:** Consider the degree of challenge, scope and learning curve of the role, not just the title.
- **Company Trajectory:** Consider the rate at which the company is growing and how much additional market there is for it to capture. This can impact career growth  related to wealth generation over multi-year periods.
- **Market:** Consider the degree to which you'll be exposed to many different markets as opposed to being focused on one specific market.
- **GTM Motions:** Consider the type of GTM motion and how that will impact your available and priority sensing mechanisms.
- **Additional Resources:** [Seven tips for picking your next job](https://theskip.substack.com/p/seven-tips-for-picking-your-first), [Product Salaries](https://www.levels.fyi/company/Google/salaries/Product-Manager/)

## Product Manager Onboarding

Product Manager onboarding, beyond any [product specific steps in your first week onboarding ticket](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding_tasks/department_product_management.md),
is defined in the Product projects [PM Onboarding issue template](https://gitlab.com/gitlab-com/Product/blob/master/.gitlab/issue_templates/PM-onboarding.md).

Onboarding issues can be tracked in the [Product Onboarding Issue Board](https://gitlab.com/gitlab-com/Product/-/boards/1283554?&label_name%5B%5D=onboarding).

Iteration on Product Management Onboarding is encouraged by all team members. To do so, create an MR against one of the above files and assign it to your manager for review and merge.

## Interviewing Product Management Candidates

A unique and important step in the interview process for Product Management candidates is our Deep Dive Interview. The goal of this interview is to understand the candidate's ability to communicate a long term vision as well as a short term MVC, both verbally during the interview itself, and written via two follow up issues. Once the issues are ready for you to read, it is an opportunity to provide feedback and see how the candidate responds to that feedback.

You can find more information and instructions on the Deep Dive interview [here](https://gitlab.com/gitlab-com/people-group/hiring-processes/blob/master/Product/DeepDive.md). For information on our hiring process, head over to our [hiring handbook pages](/handbook/hiring/). |

## Taking Time Off

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/AkNcYjWrvDU" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

Team members are [strongly recommended to take two weeks of consecutive time off a year](/handbook/paid-time-off/#a-gitlab-team-members-guide-to-time-off). Extended leave longer than the [`no ask, must tell` GitLab paid time off](/handbook/paid-time-off/#a-gitlab-team-members-guide-to-time-off) is common, especially for [parental leave](/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave). Leaving for a longer period of time can be daunting, but it is critical to ensure your `rest ethic` meets your `work ethic` and also to ensure we don't have single points of failure in the company. Here are a few suggestions for product managers to help plan for this time.

On a high level, these are the two most important things to do:

1. [Communicate your time off](/handbook/paid-time-off/#communicating-your-time-off)
1. [Create a PM coverage issue](https://gitlab.com/gitlab-com/Product/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) (details below)

### Creating a PM coverage issue

You can use [this issue template](https://gitlab.com/gitlab-com/Product/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) to define handshake responsibilites. For extended leave it is important to find one or more Directly Responsible Individuals (DRIs) that will be able to make product descisions while you are away. This may be your manager, another PM, or maybe the engineering manager for your team. The coverage issue should contain all the necessary information for the DRIs to make good decisions in your absence, so please make sure to include as much detail as needed. In addition to creating the issue, it may be useful to have a specific handover meeting to ensure that everyone is on the same page before you leave.

### Returning from Time Off

Returning from time off can be overwhelming and daunting. You should work with your DRIs to understand what has changed during your absence and what the current priorities are. Also, communicate transparently that your response time may be slower because you are catching up. Here are some additional tips on [how to return back to work after taking parental leave](/handbook/paid-time-off/#returning-to-work-after-parental-leave).

## Product Shadowing with Engineering

The Product Shadowing program is loosely based on the [CEO Shadow](/ceo/shadow/index.md) program and is designed to facilitate greater understanding between Product Managers and Engineers within a stage. The program was trialed with success in the Plan stage.

A Product Manager can host an Engineer within their stage to shadow them over two working days, or the equivalent split over multiple days to maximize experience with different functions of the role.

To propose a shadow, please create an issue in the stage issue tracker using other shadowing issues as an example ([example](https://gitlab.com/gitlab-org/plan/-/issues/118))

## Product Leadership

The separate page [Product Leadership](/handbook/product/product-leadership/) covers how to be an effective leader in the product management organization.
